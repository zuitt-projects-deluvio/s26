let http = require("http");

http.createServer((req, res) => {

	if(req.url === "/"){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('Hi! Welcome to our Homepage')
	} else if(req.url === "/login"){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('Welcome to the Login page!')
	} else {
		res.writeHead(404, {'Content-Type' : 'text/plain'})
		res.end('Resource cannot be found')
	}
}).listen(4000);

console.log('Server is running on localhost: 4000');

console.log(http)